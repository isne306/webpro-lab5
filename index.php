<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel=stylesheet href="css/style.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
body 
{
  background: url(img/bg.jpg) no-repeat center center fixed;
    -webkit-background-size: cover; /* For WebKit*/
    -moz-background-size: cover;    /* Mozilla*/
    -o-background-size: cover;      /* Opera*/
    background-size: cover;         /* Generic*/
}
</style>



<!--html starts here-->
<!DOCTYPE HTML> 
<title>18+ Website with PHP.</title>
</head>

<body>
<div id="box"><br>
<center>
<div class="title"><b>Sign Up Form</b></div>
<div ><img src="img/18.png" class="img"></div>

<form name="submit" action="home.php" method="post" onsubmit="return check(this);">
  <p>
    <div class="left">Forename</div>
    <div class="right"><input type="text" name="Forename"><span id="fn">&nbsp;</span></div>
    <br><br>
    <div class="left">Surname</div>    
    <div class="right"> <input type="text" name="Surname"><span id="sn">&nbsp;</span></div>
    <br><br>
    <div class="left">Username</div>
    <div class="right"><input type="text" name="Username"><span id="us">&nbsp;</span></div>
    <br><br>
    <div class="left">Password</div>
    <div class="right"><input type="password" name="Password"><span id="ps">&nbsp;</span></div>
    <br><br>
    <div class="left">Re-Password</div>
    <div class="right"><input type="password" name="rePassword"><span id="pr">&nbsp;</span></div>
    <br><br>
    <div class="left">Age</div>
    <div class="right"><input name="Age" type="date" maxlength="10"><span id="ag">&nbsp;</span></div>
    <br><br>
    <div class="left">Email</div>
    <div class="right"><input type="text" name="Email"><span id="em">&nbsp;</span></div><br><br>
  </p>
    <button type="submit" class="submit" name="submit">Submit</button>
</center>
</form>
</div>
</body>
</html>




<!-- 
Form Validation
	A continuation from the previous Javascript exercise. 
    This time validate the form using PHP. 
    Use the form to send data to a new page, 
    and use the new page to display an appropriate message, e.g.;
"Password must have 8 characters"
"Age must be >18″
“All data correct”

-->